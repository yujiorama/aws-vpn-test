# AWS で OpenVPN

この辺のリファレンスでやっていく。

[AWS Client VPN](https://docs.aws.amazon.com/ja_jp/vpn/latest/clientvpn-admin)

> 作ってから気付いたけどクライアント VPN エンドポイントとクライアント証明書の対応は1対1になっている。
>
> つまり、利用者ごとにクライアント証明書を準備する場合はエンドポイントも利用者と同じ数だけ必要になってしまう。
>
> Azure の VPN ゲートウェイにはそんな制約はなかったので、利用コストを概算するときは注意が必要そうだ。

## 作るリソース

* VPC
* パブリックサブネット
* プライベートサブネット
* クライアント VPN エンドポイント
* インターネットゲートウェイ
* 自己署名CA
* 自己署名サーバー証明書
* 自己署名クライアント証明書

## 構成

## コツ

### Terraform リソース

初めは [aws_vpn_gateway](https://www.terraform.io/docs/providers/aws/r/vpn_gateway.html) を使うものだと思い込んでいたけど違った。このリソースはサイト間 VPN に使用する。

クライアント VPN のリソースは [aws_ec2_client_vpn_endpoint](https://www.terraform.io/docs/providers/aws/r/ec2_client_vpn_endpoint.html) と [aws_ec2_client_vpn_network_association](https://www.terraform.io/docs/providers/aws/r/ec2_client_vpn_network_association.html) が該当する。

### Terraform だけで完結しない操作

#### [ターゲットネットワーク](https://docs.aws.amazon.com/ja_jp/vpn/latest/clientvpn-admin/cvpn-working-target.html)の構成

クライアント VPN エンドポイントでは、指定した VPC サブネットに VPC セキュリティグループを適用できる。

しかし、Terraform にはそのためのリソースがない。

なので、`aws ec2 apply-security-groups-to-client-vpn-target-network` を使う必要がある。

[vpn.tf](./vpn.tf) では [local-exec](https://www.terraform.io/docs/provisioners/local-exec.html) プロビジョナーで実行している。

#### [承認ルール](https://docs.aws.amazon.com/ja_jp/vpn/latest/clientvpn-admin/cvpn-working-rules.html)の構成

`aws ec2 authorize-client-vpn-ingress` を使う必要がある。

[vpn.tf](./vpn.tf) では [local-exec](https://www.terraform.io/docs/provisioners/local-exec.html) プロビジョナーで実行している。

#### [ルート](https://docs.aws.amazon.com/ja_jp/vpn/latest/clientvpn-admin/cvpn-working-routes.html)の構成

`aws ec2 create-client-vpn-route` を使う必要がある。

[vpn.tf](./vpn.tf) では [local-exec](https://www.terraform.io/docs/provisioners/local-exec.html) プロビジョナーで実行している。

#### OpenVPN クライアント設定ファイルの準備

AWS から取得できる OpenVPN クライアント設定ファイルにはクライアント証明書の情報が含まれていないため、利用者が自分で埋め込まなければならない。やっかいだ。

`aws ec2 export-client-vpn-client-configuration` を使う必要がある。

[vpn.tf](./vpn.tf) では [locals](https://www.terraform.io/docs/configuration/locals.html) で作成している。

### 自己署名CA、サーバー証明書、クライアント証明書

AWS のリファレンスでは `easy-rsa` を使うようになっているけど、ややこしい。

Terraform の [TLS Provider](https://www.terraform.io/docs/providers/tls/) を利用するとすべて Terraform で完結できる。

自己署名証明書で十分な用途であれば問題なく利用できそう。

具体的な使い方は [acm.tf](./acm.tf) を参照。

## 実験

bakend を構成。

```bash
terraform init -backend=true -backend-config=backend.tfvars
```

terraform を実行。

```bash
mkdir -p out && terraform plan -var-file=config.tfvars -out=out/plan.zip
terraform apply out/plan.zip
```

OpenVPN 設定ファイルを取得。Import して接続できれば成功。

```bash
terraform output client_vpn_config > aws-vpn.ovpn
```
