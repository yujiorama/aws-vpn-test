data "aws_availability_zones" "available" {
  state = "available"
  blacklisted_names = [
    "ap-northeast-1b",
  ]
}

resource "aws_vpc" "vpc" {
  cidr_block           = var.vpc_cidr
  enable_dns_hostnames = true
  enable_dns_support   = true

  tags = {
    Name = "vpn-test-vpc",
  }
}

resource "aws_subnet" "vpn" {
  vpc_id                          = aws_vpc.vpc.id
  availability_zone               = var.vpn_subnet_availability_zone
  cidr_block                      = var.vpn_subnet_cidr
  assign_ipv6_address_on_creation = false
  map_public_ip_on_launch         = false

  tags = {
    Name = "vpn-test-subnet-vpn",
  }

}

resource "aws_subnet" "public" {
  count = var.vpc_subnet_count

  vpc_id            = aws_vpc.vpc.id
  availability_zone = data.aws_availability_zones.available.names[count.index]
  cidr_block        = cidrsubnet(aws_vpc.vpc.cidr_block, 8, count.index + var.vpc_subnet_count)
  // "192.168.2${count.index}.0/24"
  assign_ipv6_address_on_creation = false
  map_public_ip_on_launch         = false

  tags = {
    Name = "vpn-test-subnet-public-[${count.index}]",
  }
}

resource "aws_internet_gateway" "igw" {
  vpc_id = aws_vpc.vpc.id

  tags = {
    Name = "vpn-test-igw-vpn",
  }
}

resource "aws_route_table" "vpn" {
  vpc_id = aws_vpc.vpc.id

  tags = {
    Name = "vpn-test-rt-vpn",
  }
}

resource "aws_route_table" "public" {
  count = var.vpc_subnet_count

  vpc_id = aws_vpc.vpc.id

  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = aws_internet_gateway.igw.id
  }

  tags = {
    Name = "vpn-test-rt-public-[${count.index}]",
  }
}

resource "aws_route_table_association" "vpn" {
  route_table_id = aws_route_table.vpn.id
  subnet_id      = aws_subnet.vpn.id
}

resource "aws_route_table_association" "public" {
  count = var.vpc_subnet_count

  route_table_id = aws_route_table.public[count.index].id
  subnet_id      = aws_subnet.public[count.index].id
}

output "vpc_id" {
  value = aws_vpc.vpc.id
}

output "vpn_subnet_ids" {
  value = aws_subnet.vpn.id
}

output "public_subnet_ids" {
  value = aws_subnet.public.*.id
}
