resource "aws_security_group" "vpn" {
  name   = "vpn-test-sg-vpn"
  vpc_id = aws_vpc.vpc.id

  egress {
    from_port = 0
    protocol  = "-1"
    to_port   = 0
    cidr_blocks = [
      "0.0.0.0/0",
    ]
  }

  tags = {
    Name = "vpn-test-sg-vpn",
  }
}

resource "aws_security_group_rule" "ingress" {
  security_group_id        = aws_security_group.vpn.id
  type                     = "ingress"
  from_port                = 0
  protocol                 = "-1"
  source_security_group_id = aws_security_group.vpn.id
  to_port                  = 0
}
