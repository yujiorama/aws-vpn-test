variable "aws_region" {
  type        = string
  description = "AWS Region"
  default     = "ap-northeast-1"
}

variable "vpc_cidr" {
  type        = string
  description = "The CIDR block of VPC"
}

variable "vpc_subnet_count" {
  type        = string
  description = "The number of subnets to create."
  default     = "3"
}

variable "vpn_subnet_availability_zone" {
  type        = string
  description = "The availability zone of subnet attached to vpn."
  default     = "ap-northeast-1a"
}

variable "vpn_subnet_cidr" {
  type        = string
  description = "The CIDR block of subnet attached to vpn."
}

variable "vpn_client_cidr" {
  type        = string
  description = "The CIDR block of vpn client."
}

variable "vpn_transport_protocol" {
  type        = string
  description = "Transport protocol of vpn."
  default     = "tcp"
}
