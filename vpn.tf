resource "aws_ec2_client_vpn_endpoint" "vpn" {
  description            = "Client vpn endpoint for OpenVPN connection."
  client_cidr_block      = var.vpn_client_cidr
  server_certificate_arn = aws_acm_certificate.server.arn
  split_tunnel           = true
  transport_protocol     = var.vpn_transport_protocol

  authentication_options {
    type                       = "certificate-authentication"
    root_certificate_chain_arn = aws_acm_certificate.client.arn
  }

  connection_log_options {
    enabled = false
  }

  tags = {
    "Name" = "vpn-test-vpn-endpoint"
  }
}

resource "aws_ec2_client_vpn_network_association" "vpn" {
  client_vpn_endpoint_id = aws_ec2_client_vpn_endpoint.vpn.id
  subnet_id              = aws_subnet.vpn.id
}

resource "null_resource" "authorize_client_vpn_ingress" {
  provisioner "local-exec" {
    when    = create
    command = "aws ec2 authorize-client-vpn-ingress --client-vpn-endpoint-id ${aws_ec2_client_vpn_endpoint.vpn.id} --target-network-cidr 0.0.0.0/0 --authorize-all-groups"
  }

  depends_on = [
    aws_ec2_client_vpn_endpoint.vpn,
  ]
}

resource "null_resource" "client_vpn_route_public" {
  provisioner "local-exec" {
    command = "aws ec2 create-client-vpn-route --client-vpn-endpoint-id ${aws_ec2_client_vpn_endpoint.vpn.id} --destination-cidr-block 0.0.0.0/0 --target-vpc-subnet-id ${aws_subnet.vpn.id}"
  }

  depends_on = [
    aws_ec2_client_vpn_endpoint.vpn,
    aws_subnet.vpn,
  ]
}

resource "null_resource" "apply_security_groups_to_client_vpn_target_network" {
  provisioner "local-exec" {
    when    = create
    command = "aws ec2 apply-security-groups-to-client-vpn-target-network --vpc-id ${aws_vpc.vpc.id} --client-vpn-endpoint-id ${aws_ec2_client_vpn_endpoint.vpn.id} --security-group-ids ${aws_security_group.vpn.id}"

  }

  depends_on = [
    aws_ec2_client_vpn_endpoint.vpn,
    aws_ec2_client_vpn_network_association.vpn,
    aws_security_group.vpn,
  ]
}

locals {
  client_vpn_config = <<VPNCONFIG
client
dev tun
proto tcp
remote ${aws_ec2_client_vpn_endpoint.vpn.dns_name} 443
remote-random-hostname
resolv-retry infinite
nobind
persist-key
persist-tun
remote-cert-tls server
cipher AES-256-GCM
verb 3
<ca>
${tls_self_signed_cert.ca.cert_pem}
</ca>
reneg-sec 0
<cert>
${tls_locally_signed_cert.client.cert_pem}
</cert>
<key>
${tls_private_key.client.private_key_pem}
</key>
VPNCONFIG
}

output "client_vpn_config" {
  value = local.client_vpn_config
}
